FROM mcr.microsoft.com/dotnet/core/sdk:3.1-bionic

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime \
 && dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get install python3.5 python3-pip zip awscli jq -y \
  && pip3 install boto3
